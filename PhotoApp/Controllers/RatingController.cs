using System;
using System.Web.Mvc;
using PhotoApp.Models;

namespace MvcApplication1.Controllers
{
    public class RatingController : Controller
    {
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PostRating(int rating, Guid? imageId)
        {
            new PhotoModel().SetRating(rating, imageId);
            return Json("You rated this " + rating.ToString() + " star(s)");
        }
    }
}
