﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using PhotoApp.Models;

namespace PhotoApp.Controllers
{
    public class PhotoController : Controller
    {
        //
        // GET: /Photo/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RandomPhoto()
        {
            Photo[] model = new Photo[7];
            PhotoModel aa = new PhotoModel();

            for (int i = 0; i < 6; i++)
            {
                model[i] = aa.GetRandomPhoto(i);
            }

            return PartialView(model);
        }

        public ActionResult GenerateImage(int i)
        {
            PhotoModel aa = new PhotoModel();
            return new FileContentResult(aa.GetRandomPhoto(i).Image, "image/jpeg");
        }

        public ActionResult AlbumPhotos(Guid albumId)
        {
            if (new AlbumFolderModel().UserNameFromAlbumId(albumId) == User.Identity.Name)
            {
                Photo[] model = new Photo[100];
                PhotoModel aa = new PhotoModel();

                for (int i = 0; i < 100; i++)
                {
                    model[i] = aa.GetAlbumPhoto(i, albumId);
                    if (model[i] == null)
                    {
                        ViewBag.numberOfPhotosInAlbum = i;
                        ViewBag.albumId = albumId;
                        break;
                    }
                }
                return View(model);
            }
            else
            {
                return RedirectToAction("UserLogin", "User");
            }
        }

        public ActionResult GenerateAlbumImage(int i, Guid albumId)
        {
            if (new AlbumFolderModel().UserNameFromAlbumId(albumId) == User.Identity.Name)
            {
                return new FileContentResult(new PhotoModel().GetAlbumPhoto(i, albumId).Image, "image/jpeg");
            }
            return RedirectToAction("UserLogin", "User");
        }

        [HttpGet]
        public ActionResult AddingPhoto(Guid? albumId)
        {
            if (new AlbumFolderModel().UserNameFromAlbumId(albumId) == User.Identity.Name)
            {
                ViewBag.AlbumId = albumId;
                return View();
            }
            else
            {
                return RedirectToAction("UserLogin", "User");
            }
        }

        [HttpPost]
        public ActionResult AddingPhoto(HttpPostedFileBase[] photoFile, Photo photo)
        {
            for (int i = 0; i < 20; i++)
            {
                try
                {//end of the photos detected (foreach  don't work properly)
                    //here catch out of the bounds exception
                    var a = photoFile[i] != null;
                }
                catch (Exception)
                {
                    return RedirectToAction("AlbumPhotos", "Photo", new { albumId = photo.AlbumId });
                }
                if (photoFile[i] != null && photoFile[i].ContentLength > 0 && photo.Name != null && photo.Name.Length > 5 && photo.Name.Length < 100)
                {
                    try
                    {
                        if (!(new PhotoModel().AddPhoto(photoFile[i], photo)))
                        {
                            ModelState.AddModelError("", "Is it really an image?");
                            return RedirectToAction("AlbumPhotos", "Photo", new { albumId = photo.AlbumId });
                        }
                    }
                    catch
                    {
                        ModelState.AddModelError("", "An error when uploading");
                        return View(photo);
                    }
                }
                else
                {
                    ViewBag.albumId = photo.AlbumId;
                    if (photoFile[i] != null && photoFile[i].ContentLength > 0)
                        ModelState.AddModelError("", "Choose the correct name, please");
                    else
                        ModelState.AddModelError("", "Choose the correct image, please");
                    return View(photo);
                }
            }
            return RedirectToAction("AlbumPhotos", "Photo", new { albumId = photo.AlbumId });
        }

        public ActionResult DeletePhoto(Guid photoId)
        {
            if (new AlbumFolderModel().UserNameFromPhotoId(photoId) == User.Identity.Name)
            {

                new PhotoModel().DeletePhoto(photoId);
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                return RedirectToAction("UserLogin", "User");
            }
        }
    }
}
