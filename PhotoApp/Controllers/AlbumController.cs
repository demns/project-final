﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PhotoApp.Models;

namespace PhotoApp.Controllers
{
    public class AlbumController : Controller
    {
        //
        // GET: /Album/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListOfAlbums()
        {
            AlbumFolderModel a = new AlbumFolderModel();
            return PartialView(a.AlbumList(User.Identity.Name));
        }

        [HttpGet]
        public ActionResult NewAlbum()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewAlbum(AlbumFolder album)
        {
            var a = new AlbumFolderModel();
            Guid? albumIdent;
            try
            {
                albumIdent = a.NewAlbum(User.Identity.Name, album.AlbumTitle);
                if (albumIdent == null)
                    ModelState.AddModelError("", "Error");
                else
                {
                    return RedirectToAction("AlbumPhotos", "Photo", new { albumId = albumIdent });
                }

            }
            catch
            {
                ModelState.AddModelError("", "Wrong Data");
            }
            return View();
        }

        public ActionResult DeleteAlbum(Guid albumId)
        {
            if (new AlbumFolderModel().UserNameFromAlbumId(albumId) == User.Identity.Name)
            {
                new AlbumFolderModel().DeleteAlbum(albumId);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("UserLogin", "User");
            }
        }
    }
}
