﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhotoApp.Models
{
    [MetadataType(typeof(AlbumFolderModel))]
    public partial class AlbumFolder
    {

    }

    public class AlbumFolderModel
    {
        [Display(Name = "User Identification")]
        public Guid UserId { get; set; }

        [Display(Name = "Album Identification")]
        public Guid AlbumId { get; set; }

        [StringLength(20, MinimumLength = 6)]
        [Required]
        [Display(Name = "Album Title")]

        public string AlbumTitle { get; set; }


        private MainTableEntities db;
        public AlbumFolderModel()
        {
            db = new MainTableEntities();
        }

        public ICollection<AlbumFolder> AlbumList(string user)
        {
            return db.Users.FirstOrDefault(u => u.Name == user).AlbumFolders;
        }

        public Guid? NewAlbum(string user, string albumTitle)
        {
            var userName = db.Users.FirstOrDefault(u => u.Name == user);
            if (userName != null && albumTitle != null)
            {
                var sysAlbum = db.AlbumFolders.Create();
                sysAlbum.AlbumTitle = albumTitle;
                sysAlbum.User = userName;
                sysAlbum.AlbumId = Guid.NewGuid();
                db.AlbumFolders.Add(sysAlbum);
                db.SaveChanges();
                return db.AlbumFolders.FirstOrDefault(al => al.AlbumTitle == albumTitle).AlbumId;
            }
            return null;
        }

        public bool DeleteAlbum(Guid albumId)
        {
            try
            {
                var albumToDelete = db.AlbumFolders.FirstOrDefault(al => al.AlbumId == albumId);
                db.AlbumFolders.Remove(albumToDelete);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public string UserNameFromAlbumId(Guid albumId)
        {
            try
            {
                return db.AlbumFolders.FirstOrDefault(al => al.AlbumId == albumId).User.Name;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string UserNameFromAlbumId(Guid? albumId)
        {
            try
            {
                return db.AlbumFolders.FirstOrDefault(al => al.AlbumId == albumId).User.Name;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public string UserNameFromPhotoId(Guid photoId)
        {
            try
            {
                return db.Photos.FirstOrDefault(ph => ph.ImageId == photoId).AlbumFolder.User.Name;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}