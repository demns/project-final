﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;

namespace PhotoApp.Models
{
    [MetadataType(typeof(PhotoModel))]
    public partial class Photo
    {

    }

    public class PhotoModel
    {
        [Required]
        [Display(Name = "Album Identification")]
        public Guid AlbumId { get; set; }

        [Required]
        public byte[] Image { get; set; }

        [Display(Name = "Rating of the photo")]
        public double Rating { get; set; }

        [Display(Name = "Views of this photo")]
        public int Views { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 6)]
        [Display(Name = "Name of the photo")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Image Identification")]
        public Guid ImageId { get; set; }

        private MainTableEntities db;
        public PhotoModel()
        {
            db = new MainTableEntities();
        }

        public Photo GetRandomPhoto(int i)
        {
            var photo = new Photo();
            try
            {
                photo = db.Photos.OrderByDescending(ph => ph.Rating).Skip(i).FirstOrDefault();
            }
            catch
            {
                return null;
            }
            return photo;
        }

        public Photo GetAlbumPhoto(int i, Guid albumId)
        {//todo guid photo
            var photo = new Photo();
            try
            {
                photo = db.Photos.Where(album => album.AlbumId == albumId).OrderByDescending(ph => ph.Rating).Skip(i).FirstOrDefault();
            }
            catch
            {
                return null;
            }
            return photo;
        }

        public bool AddPhoto(HttpPostedFileBase photoFile, Photo photo)
        {
            if (photoFile.ContentType == "image/png" || photoFile.ContentType == "image/jpeg")
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    photoFile.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                    try
                    {
                        var sysPhoto = db.Photos.Create();
                        sysPhoto.Name = photo.Name;
                        sysPhoto.AlbumId = photo.AlbumId;
                        sysPhoto.Image = array;
                        sysPhoto.Rating = 1;
                        sysPhoto.Views = 1; //I had a lot of the photo properties
                        sysPhoto.ImageId = Guid.NewGuid();
                        db.Photos.Add(sysPhoto);
                        db.SaveChanges();
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
            else return false;

            return true;
        }

        public bool DeletePhoto(Guid photoId)
        {
            try
            {
                var photoToDelete = db.Photos.FirstOrDefault(ph => ph.ImageId == photoId);
                db.Photos.Remove(photoToDelete);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool SetRating(int newRating, Guid? photoId)
        {
            try
            {
                db.Photos.FirstOrDefault(ph => ph.ImageId == photoId).Rating = newRating;
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}