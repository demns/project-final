﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhotoApp.Models
{
    [MetadataType(typeof(UserModel))]
    public partial class User
    {
        
    }

    public class UserModel
    {
        [Required]
        [StringLength(100)]
        [Display(Name = "User name: ")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6)]
        [Display(Name = "Password: ")]
        public string Password { get; set; }

        private MainTableEntities db;
        public UserModel()
        {
            db = new MainTableEntities();
        }

        public bool IsValid()
        {
            var user = db.Users.FirstOrDefault(u => u.Name == this.Name);
            return (user != null) && (this.Password.Equals(user.Password.Trim()));
        }
}

    public class UserModelRegistration: UserModel
    {
        [StringLength(20, MinimumLength = 2)]
        [Display(Name = "Captcha: ")]
        public string Captcha { get; set; }

        private MainTableEntities db;
        public UserModelRegistration()
        {
            db = new MainTableEntities();
        }

        public bool Register()
        {
            var userName = db.Users.FirstOrDefault(u => u.Name == this.Name);
            if (userName != null)
            {
                return false;
            }
            var sysUser = db.Users.Create();
            sysUser.Name = this.Name;
            sysUser.Password = this.Password;
            sysUser.UserId = Guid.NewGuid(); 
            db.Users.Add(sysUser);
            db.SaveChanges();
            return true;
        }
    }
}