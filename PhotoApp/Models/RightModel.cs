﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhotoApp.Models
{
    [MetadataType(typeof(RightModel))]
    public partial class Right
    {

    }
    public class RightModel
    {
        [Required]
        [Display(Name = "User Identification")]
        public Guid UserId { get; set; }

        [Required]
        [Display(Name = "User Rights")]
        public int Rights { get; set; }
    }
}